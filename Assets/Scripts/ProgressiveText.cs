using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressiveText : MonoBehaviour
{
    [System.Serializable]
    public class ProgressiveMessage
    {
        public string text;
        public float charPause = 0.1f;
        public float afterPause = 0;
        public bool fadeIn;
        public bool fadeOut;
    }

    public ProgressiveMessage[] messages;
    public GameObject afterObject;
    public bool autostart;

    Text txt;
    Image fade;
    float startTime;
    float t;

    void Start()
    {
        if (autostart)
        {
            Setup();
            StartCoroutine(AutoPlay());
        }
    }

    public void Setup()
    {
        txt = gameObject.GetComponent<Text>();

        // Clear the GUI text
        txt.text = "";
    }

    public IEnumerator AutoPlay()
    {
        foreach (ProgressiveMessage m in messages)
        {
            yield return StartCoroutine(TypeLetters(m.text, m.charPause, m.afterPause));
        }

        // Trigger after object
        if (afterObject != null)
        {
            afterObject.SetActive(true);
        }
    }

    public IEnumerator TypeLetters(string text, float charPause=0.1f, float afterPause=1f)
    {
        // Iterate over each letter
        foreach (char letter in text.ToCharArray())
        {
            txt.text += letter; // Add a single character to the GUI text
            yield return new WaitForSeconds(charPause);
        }

        // After pause
        if (afterPause >= 0)
            yield return new WaitForSeconds(afterPause);            

        // Reset
        txt.text = "";
    }
}