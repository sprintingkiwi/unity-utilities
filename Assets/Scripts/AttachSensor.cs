﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttachSensor : MonoBehaviour
{
    [Header("Options")]
    public string targetName;
    public UnityEvent callback;

    [Header("System")]
    public bool touching;
    public GameObject touchedObject;

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.name == targetName)
        {
            touching = true;
            touchedObject = collision.gameObject;
            callback.Invoke();
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == targetName)
        {
            touching = false;
            touchedObject = null;
        }
    }
}
