﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Draggable : MonoBehaviour
{
    [Header("Options")]
    public UnityEvent OnStartDrag;
    public UnityEvent OnEndDrag;

    [Header("System")]
    public bool dragged;

    Vector3 dist;
    float posX;
    float posZ;
    float posY;

    void OnMouseDrag()
    {
        float disX = Input.mousePosition.x - posX;
        float disY = Input.mousePosition.y - posY;
        float disZ = Input.mousePosition.z - posZ;
        Vector3 lastPos = Camera.main.ScreenToWorldPoint(new Vector3(disX, disY, disZ));
        transform.position = new Vector3(lastPos.x, lastPos.y, lastPos.z);
    }

    void OnMouseDown()
    {
        dragged = true;

        if (OnStartDrag != null)
            OnStartDrag.Invoke();

        dist = Camera.main.WorldToScreenPoint(transform.position);
        posX = Input.mousePosition.x - dist.x;
        posY = Input.mousePosition.y - dist.y;
        posZ = Input.mousePosition.z - dist.z;
    }

    void OnMouseUp()
    {
        // Ended Drag
        dragged = false;

        if (OnEndDrag != null)
            OnEndDrag.Invoke();
    }
}
