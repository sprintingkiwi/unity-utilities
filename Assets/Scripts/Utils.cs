
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;


public static class Utils {

    public static void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public static void PlaySound(string clipName, bool loop = false, float volume = 1)
    {
        GameObject audio = new GameObject(clipName);
        AudioSource audioSource = audio.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load("Audio/" + clipName, typeof(AudioClip)) as AudioClip;
        audioSource.loop = loop;
        audioSource.volume = volume;
        audioSource.Play();
        GameObject.Destroy(audio, audioSource.clip.length);
    }
    public static void PlaySound(AudioClip clip, bool loop = false, float volume = 1)
    {
        GameObject audio = new GameObject(clip.name);
        AudioSource audioSource = audio.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.loop = loop;
        audioSource.volume = volume;
        audioSource.Play();
        GameObject.Destroy(audio, audioSource.clip.length);
    }

    public static IEnumerator WaitEndOfAudio(AudioSource source)
    {
        if (source == null)
            yield break;

        while (source.isPlaying)
        {
            // Interrupt audio pressing Space Bar
            if (Input.GetKeyDown(KeyCode.Space))
            {
                source.Stop();
                GameObject.Destroy(source.gameObject);
                break;
            }
            yield return null;
        }

        yield return null;
    }

    public static Vector3 RandomVector3()
    {
        return new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100)).normalized;
    }

    public static IEnumerator ProgessiveWrite(ProgressiveText pt, string text, float charPause = 0.01f, float afterPause = 1f, bool fadeIn = false, bool fadeOut = false)
    {
        pt.Setup();
        yield return pt.StartCoroutine(pt.TypeLetters(text, charPause, afterPause));
    }

    public static bool WriteFile(string path, string data, string fileName)
    {
        bool retValue = false;
        try
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            System.IO.File.WriteAllText(path + fileName, data);
            retValue = true;
        }
        catch (System.Exception ex)
        {
            string ErrorMessages = "File Write Error\n" + ex.Message;
            retValue = false;
            Debug.LogError(ErrorMessages);
        }
        return retValue;
    }

    public static Vector3 CharPosition(Text textComp, int charIndex)
    {
        Debug.Log("Finding position of #" + charIndex.ToString() + " character in text " + textComp.name);

        string text = textComp.text;

        float screenScaleFactor = GameObject.Find("Canvas").GetComponent<Canvas>().scaleFactor;

        if (charIndex >= text.Length)
            return Vector3.zero;

        TextGenerator textGen = new TextGenerator(text.Length);
        Vector2 extents = textComp.gameObject.GetComponent<RectTransform>().rect.size;
        textGen.Populate(text, textComp.GetGenerationSettings(extents));

        int newLine = text.Substring(0, charIndex).Split('\n').Length - 1;
        int whiteSpace = text.Substring(0, charIndex).Split(' ').Length - 1;
        //int indexOfTextQuad = (charIndex * 4) + (newLine * 4) - 4; // This line is for Unity 2017
        int indexOfTextQuad = (charIndex * 4) + (newLine * 4) - 4 - whiteSpace*4; // Fixed this line for Unity 2019
        if (indexOfTextQuad < textGen.vertexCount)
        {
            Vector3 avgPos = (textGen.verts[indexOfTextQuad].position +
                textGen.verts[indexOfTextQuad + 1].position +
                textGen.verts[indexOfTextQuad + 2].position +
                textGen.verts[indexOfTextQuad + 3].position) / 4f;

            //Debug.Log(avgPos);

            // Either comment one of the following based on canvas scale option:
            //Vector3 testPoint = avgPos; // Canvas constant pixel size compatibility
            Vector3 testPoint = avgPos / screenScaleFactor; // Canvas scale with screen compatibility

            Vector3 worldPos = textComp.transform.TransformPoint(testPoint);
            Debug.Log(worldPos);
            new GameObject("point").transform.position = worldPos;
            Debug.DrawRay(worldPos, Vector3.up, Color.red, 50f);

            return worldPos;
        }
        else
        {
            Debug.LogError("Out of text bound");
            return Vector3.zero;
        }
    }

    public static string GetTextWithoutMarkup(string text)
    {
        bool loop = false;
        string ret = "";
        foreach (char x in text)
        {
            if (x == '<')
            {
                loop = true;
                continue;
            }
            else if (x == '>')
            {
                loop = false;
                continue;
            }
            else if (loop)
            {
                continue;
            }
            ret += x;
        }
        return ret;
    }
    

    public class WaitCondition
    {
        public System.Func<bool> condition;
        public IEnumerator warningAction;
    }
    public static IEnumerator sampleWarningAction(string text, InputField field)
    {
        field.image.color = Color.red;
        yield return new WaitForSeconds(1);
        field.image.color = Color.white;

        yield return null;
    }

}
