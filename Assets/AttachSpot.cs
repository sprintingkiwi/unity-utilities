﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachSpot : MonoBehaviour
{
    public void AttachObject()
    {
        AttachSensor spotSensor = gameObject.GetComponentInChildren<AttachSensor>();
        spotSensor.touchedObject.transform.position = transform.position;
    }
}
